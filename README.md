## Distro Details

Linux version 4.4.0-53-generic (buildd@lcy01-28) (gcc version 5.4.0 20160609 (Ubuntu 5.4.0-6ubuntu1~16.04.4) ) #74-Ubuntu SMP Fri Dec 2 15:59:10 UTC 2016

## Configure `rsyslog`

uncomment these 2 lines in `/etc/rsyslog.conf`

```
# provides UDP syslog reception
module(load="imudp")
input(type="imudp" port="514")
```

## Restart `rsyslog`

```
sudo service rsyslog restart
```

## Build and run app

```
./run-with-build.sh
```

## Tail `syslog`

```
tail -f /var/log/syslog
```

## Expected output

```
Aug 11 10:48:34 asus3 testlogback debug message slf4j thread:main priority:DEBUG category:TestLogback exception:
Aug 11 10:48:34 asus3 testlogback info message slf4j thread:main priority:INFO category:TestLogback exception:
Aug 11 10:48:34 asus3 testlogback warn message slf4j thread:main priority:WARN category:TestLogback exception:
Aug 11 10:48:34 asus3 testlogback error message slf4j thread:main priority:ERROR category:TestLogback exception:
Aug 11 10:48:34 asus3 testlogback error message with stack trace slf4j thread:main priority:ERROR category:TestLogback exception:java.lang.Exception: I forced this exception#012#011at TestLogback.main(TestLogback.java:16)#012Caused by: java.lang.ArithmeticException: / by zero#012#011at TestLogback.main(TestLogback.java:14)
```
