import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestLogback {

    static final Logger logger = LoggerFactory.getLogger(TestLogback.class);

    public static void main(String[] args) throws Exception {
        logger.debug("debug message slf4j");
        logger.info("info message slf4j");
        logger.warn("warn message slf4j");
        logger.error("error message slf4j");
        try {
            int i = 1/0;
        } catch(Exception exc) {
            logger.error("error message with stack trace slf4j",
                    new Exception("I forced this exception",exc));
        }
    }    

}
